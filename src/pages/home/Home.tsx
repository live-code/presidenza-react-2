// example component based approach
// Home: Smart Component
// Children: Stateless components
import React, { useState } from 'react';
import { HomeCommandBar } from './components/HomeCommandBar';
import { HomeDisplayData } from './components/HomeDisplayData';
import { useHome } from './hooks/useHome';

export interface Item {id: number; name: string}

export const Home: React.VFC = () => {
  const { list, actions } = useHome();

  return <div>
    <HomeCommandBar
      onAddItem={actions.addItem}
      onClear={actions.clear}
    />
    <hr/>
    <HomeDisplayData
      data={list}
      onDeleteItem={actions.deleteItem}
    />
  </div>
}
