import React  from 'react';
import { Item } from '../Home';

interface HomeCommandBarProps {
  onClear: () => void;
  onAddItem: (item: Item) => void;
}

export const HomeCommandBar: React.FC<HomeCommandBarProps> = (props) => {

  function addItemHandler() {
    // varie operazioni
    props.onAddItem({id: Math.random(), name: 'pluto'});
  }

  return <div>
    <input type="text"/>
    <input type="text"/>
    <input type="text"/>
    <button onClick={addItemHandler}>Add Item</button>
    <button onClick={props.onClear}>Clear</button>
  </div>
};
