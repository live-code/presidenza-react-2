import React  from 'react';
import { Item } from '../Home';

interface HomeDisplayDataProps {
  data: Item[];
  onDeleteItem: (id: number) => void;
}
export const HomeDisplayData: React.FC<HomeDisplayDataProps> = (props) => {
  const { data, onDeleteItem } = props;

  return <div>
    <div>ci sono {data?.length} prodotti</div>
    {
      data?.map(item => {
        return (
          <li key={item.id}>
            {item.name}
            <button onClick={ () => onDeleteItem(item.id) }>del</button>
          </li>
        )
      })
    }
  </div>
};
