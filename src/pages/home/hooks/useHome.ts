import { useState } from 'react';
import { Item } from '../Home';

const initialState: Item[] = [
  { id: 1, name: 'pippo'},
  { id: 2, name: 'ciccio'},
]

export function useHome() {
  const [list, setList] = useState<Item[]>(initialState)

  function deleteItem(id: number) {
    setList(list.filter(item => item.id !== id));
  }
  const clear = () => setList([]);
  const addItem = (item: Item) => setList([...list, item]);

  return {
    list,
    actions: {
      clear,
      addItem,
      deleteItem,
    }
  };
}
