import React from 'react';
import { useAdmin } from './hooks/useAdmin';

export const Admin: React.VFC = () => {
  const { list, actions } = useAdmin();

  return <div>
    {
      list?.map(item => {
        return <li key={item.id}>{item.name}</li>
      })
    }
    <button onClick={actions.reset}>reset</button>
  </div>
}
