import { useEffect, useState } from 'react';
import axios from 'axios';

export function useAdmin() {
  const [list, setList] = useState<any[]>([])

  useEffect(() => {
    axios.get<any[]>('http://localhost:3001/products')
      .then(res => setList(res.data))
  }, []);

  function reset() {
    setList([])
  }

  return {
    list,
    actions: {
      reset
    }
  };
}
