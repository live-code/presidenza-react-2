import React, { useState } from 'react';
import axios from 'axios';
import { Auth } from '../../model/auth';
import { login } from '../../core/auth/auth.service';
import { useHistory } from 'react-router-dom';

interface FormDataProps {
  username: string;
  password: string;
}

const initialState: FormDataProps = { username: '', password: ''};

const Login: React.VFC = (props) => {
  const [formData, setFormData] = useState<FormDataProps>(initialState);
  const [error, setError] = useState<boolean>(false)
  const history = useHistory();

  function loginHandler() {
    setError(false)
    login()
      .then(res => history.replace('home'))
      .catch(err => setError(true))
  }

  function changeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    })
  }

  const valid = formData.username.length > 0 && formData.password.length > 0;

  return <div>
    { !valid && <div>Not valid</div>}
    { error && <div>Ahia! errore</div>}
    <input type="text" value={formData?.username} onChange={changeHandler} name="username" />
    <input type="text" value={formData?.password} onChange={changeHandler} name="password" />
    <button onClick={loginHandler}>Login</button>
  </div>
}
export default Login;
