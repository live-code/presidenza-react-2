import React  from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { removeItemFromLocalStorage } from './utils/localstorage.helper';
import { IfLogged } from './auth/IfLogged';

export const NavBar: React.FC = () => {
  console.log('render: navbar')
  const history = useHistory();

  const logoutHandler = () => {
    history.push('login');
    removeItemFromLocalStorage('token')
  }

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <NavLink to="/login">React Auth</NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/login">Login</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/home">Home</NavLink>
          </li>

          <IfLogged>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin">Admin</NavLink>
            </li>

            <li className="nav-item" onClick={logoutHandler}>
              <div className="nav-link">Logout</div>
            </li>
          </IfLogged>
        </ul>
      </div>
    </nav>
  )
};
