import React from 'react';
import { isLogged } from './auth.service';
import { Redirect, Route, RouteProps } from 'react-router-dom';

interface PrivateRouteProps {
  role: string;
}
export const PrivateRoute: React.FC<RouteProps & PrivateRouteProps> = ({ children, ...rest }) => {
  // console.log(rest.role)
  return <Route {...rest}>
    {isLogged() ?
      children :
      <Redirect to="/login" />
    }
  </Route>
}
