import { BrowserRouter as Router } from 'react-router-dom';
import React from 'react';
import { useInterceptor } from './useInterceptor';

export const HttpErrorMsg = () => {
  const { error, response } = useInterceptor();
  console.log(error, response)

  return (
    <>
      { error && <div className="alert alert-danger">errore http</div> }
    </>
  )

}
