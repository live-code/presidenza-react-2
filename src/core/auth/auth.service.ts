import axios from 'axios';
import { Auth } from '../../model/auth';
import { getItemFromLocalStorage, setItemInLocalStorage } from '../utils/localstorage.helper';

export function login() {
  return axios.get<Auth>('http://localhost:3001/login')
    .then(res => {
      setItemInLocalStorage('token', res.data.accessToken)
      return res.data
    })
}

export function isLogged(): boolean {
  return !!getItemFromLocalStorage('token');
}

