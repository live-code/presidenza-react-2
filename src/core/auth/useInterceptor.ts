import { useEffect, useState } from 'react';
import axios from 'axios';
import { getItemFromLocalStorage } from '../utils/localstorage.helper';

export const useInterceptor = () => {
  const [response, setResponse] = useState<any>(null);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    // interceptor
    axios.interceptors.request.use( (config) => {
      const res = {
        ...config,
        headers: {
          ...config.headers,
          'Authentication-JWT': 'Bearer ' + getItemFromLocalStorage('token')
        }
      };

      setError(false);
      setResponse(res);
      return res;
    });

// handle errors
    axios.interceptors.response.use( (response) => {
      console.log(response)
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      setError(true);
      return Promise.reject(error);
    });

  }, [])

  return {
    error,
    response
  }
}
