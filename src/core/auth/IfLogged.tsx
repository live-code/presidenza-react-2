import React from 'react';
import { isLogged } from './auth.service';

export const IfLogged: React.FC = props => {
  return isLogged() ? <>{props.children}</> : null;
}
