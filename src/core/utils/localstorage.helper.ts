export function setItemInLocalStorage(key: string, value: string) {
  localStorage.setItem(key, value);
}
export function getItemFromLocalStorage(key: string): string | null {
  return localStorage.getItem(key)
}
export function removeItemFromLocalStorage(key: string) {
  localStorage.removeItem(key)
}
