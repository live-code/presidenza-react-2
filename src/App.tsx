import React, { lazy, Suspense } from 'react';
import { Admin } from './pages/admin/Admin';
import { NavBar } from './core/NavBar';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { Home } from './pages/home/Home';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { HttpErrorMsg } from './core/auth/HttpErrorMsg';

const Login = lazy(() => import('./pages/login/Login'))

export default function App() {
  return (
    <Router>
      <HttpErrorMsg />

      <Route path="*" component={NavBar} />

      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          {/*<PrivateRoute path="/admin"><Admin /></PrivateRoute>*/}
          <PrivateRoute path="/admin" role="10">
            <Admin />
          </PrivateRoute>
          <Route path="/home" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="*">
            <Redirect to="login" />
          </Route>
        </Switch>
      </Suspense>
    </Router>
  );
}
